create table movie(
    movie_id int primary key auto_increment,
    movie_title varchar(50),
    movie_release_date date,
    movie_time time,
    director_name varchar(50)
);

insert into movie values(default, 'abc', '2020-12-01', '12:00:00', 'cba');
insert into movie values(default, 'xyz', '2021-02-01', '01:00:00', 'zyx');